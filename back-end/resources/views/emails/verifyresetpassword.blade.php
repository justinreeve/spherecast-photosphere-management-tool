<p>
	Someone (hopefully you) has requested a password reset on <a href="{{ $site_host }}">{{ $site_host }}</a>.
	If you did not initiate this request, you may ignore this message. Otherwise, please visit the
	following URL to receive a new password:
</p>
<p>
	<a href="{{ $reset_url }}">{{ $reset_url }}</a>
</p>