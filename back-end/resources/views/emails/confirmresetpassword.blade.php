<p>
	Your password on <a href="{{ $site_host }}/login">{{ $site_host }}</a> has been reset. Please log in
	with the following credentials:
<p>

<p>
	Email: <b>{{ $email }}</b><br />
	Password: <b>{{ $password }}</b>
</p>