<?php
return [
	'tmp_path' => '/var/www/photospheres.org/temp',
	'upload_path' => '/var/www/photospheres.org/files',
	'thumb_path' => '/var/www/photospheres.org/thumbs',
//	'logo_path' => '/var/www/photospheres.org/logos',
	'imported_path' => '/var/www/photospheres.org/imported_files',
	'memory_limit_mb' => 192,
	'thumb_default_width' => 320,
	'thumb_height_divisor' => 1.6,
];
?>
