<?php
return [
	'tmp_path' => '/var/www/dev.sphcst.com/temp',
	'upload_path' => '/var/www/dev.sphcst.com/files',
	'thumb_path' => '/var/www/dev.sphcst.com/thumbs',
//	'logo_path' => '/var/www/dev.sphcst.com/logos',
	'imported_path' => '/var/www/dev.sphcst.com/imported_files',
	'memory_limit_mb' => 192,
	'thumb_default_width' => 320,
	'thumb_height_divisor' => 1.6,
];
?>
