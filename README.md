# Spherecast Photosphere Management Tool #

This is a tool built in Laravel, AngularJS, and Three.js which powers https://sphcst.com and provides easy uploading and sharing of 360 photospheres. Images can be displayed in monoscopic or SBS mode for VR viewers like Google Cardboard. Sharing provides direct links, embed codes, and QR codes. Extra editing features can be turned on for corporate users where they can change the short URLs of their photosphere scenes, add titles, and even run their own private photosphere-sharing site as a white labeled service on their own domain or subdomain.
